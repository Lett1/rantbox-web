CodeMirror.defineSimpleMode("rant", {
    start: [
      // Comment
      {regex: /#.*/, token: "comment"},
      // Escape
      {regex: /\\(?:\d+,)?(?:[^u\s\r\n]|u[0-9a-f]{4})/, token: "string-2"	},
      // Constant literal
      {regex: /("(?:(?:[^"]|"")*)?")/, token: "string"},
      // Query
      {regex: /<[\s\S]+?>/g, token: "tag"},
      // Regex
      {regex: /`(?:.*?[^\\])?`i?/ig, token: "string-2"},
      // Function
      // {
      //   regex: new RegExp("(\\[)(\\$\\w+|" + rantFunctions + ")[:\\]]", "i"),
      //   token: [null, "atom"]
      // },
      // Subroutine definition
      {regex: /(\[)(\$\??)(\[.*?\])(?=\s*\:)/g, token: [null, "def", "def", "def"]}
    ]
});

function resetRunButton()
{
	$("#spinner").hide()
	$("#submit").text("Run");
	$("#output").removeClass("error");
}

function resetSaveButton()
{
	$("#spinner").hide()
	$("#savebutton").text("Save");
}

requestPending = false;
//SET THIS TO YOUR API HOST
api_url = "http://localhost:5555"


$(function() {
	myCodeMirror = CodeMirror.fromTextArea(document.getElementById("texteditor"), {
		value: "<name-male> likes to <verb-transitive> <noun.pl> with <pro.dposs-male> pet <noun-animal> on <noun.pl  -dayofweek>.",
		mode:  "rant",
		lineNumbers: true,
	});

	if(window.location.href.indexOf("#") > -1)
	{
		hash = window.location.href.split("#").slice(-1)[0];

		$.ajax({
				type: "GET",
				url: api_url+ '/api/pattern/get/' + hash, 
				dataType: "json",
				success: function(response){
						myCodeMirror.setValue(response["pattern"])
					},
				error: function(xhr, type){
					myCodeMirror.setValue("404 - This Pattern does not exist")
				}
				});
	}

	$( "#submit" ).click(function() {
		if(!requestPending)
		{
			requestPending = true;
			$("#spinner").show();
			$("#submit").text("Running...")

			$.ajax({
				type: "POST",
				url: api_url+ '/api/rant', 
				data: JSON.stringify({ pattern: myCodeMirror.getValue(), includeHidden: $("#nsfw").is(":checked") ? "nsfw" : ""}), 
				dataType: "json",
				contentType: "application/json",
				success: function(response){
						resetRunButton();
						requestPending = false;
						$("#output").empty()
						for(var key in response.output) {
							if(response.output.hasOwnProperty(key)) {
								$("#output").append("<h4>Channel: " + key + "</h4>");
								$("#output").append('<div class="output-area">' + response.output[key] + "</div>");
							}
							
						}
					},
				error: function(xhr, type){
					resetRunButton();
					requestPending = false;
					var data = xhr.responseJSON;
					$("#output").text(data.statusMessage);
					$("#output").addClass("error");
				}
				});
		}

	});

	$( "#savebutton" ).click(function() {
		if(!requestPending)
		{
			requestPending = true;
			$("#spinner").show();
			$("#savebutton").text("Saving...")

			$.ajax({
				type: "POST",
				url: api_url + '/api/pattern/save', 
				data: JSON.stringify({ pattern: myCodeMirror.getValue()}),
				dataType: "text",
				contentType: "application/json",
				success: function(response){
						resetSaveButton();
						requestPending = false;
						window.open("#"+ response, "_self");
					},
				error: function(xhr, type){
					resetSaveButton();
					requestPending = false;
					var data = xhr.responseJSON;
					$("#output").text(data.statusMessage);
					$("#output").addClass("error");
				}
				});
		}

	});
})

